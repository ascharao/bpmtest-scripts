package BDDTest;

import cucumber.api.java.en.*;
import cucumber.runtime.PendingException;

public class StepDefs {
	private SeleniumTest script;

public void setUpWebDriver() throws Exception {
   script = new SeleniumTest();
   script.setUp();
   script.efetua_login();
}
	
public void selecionaProcesso() throws Exception {
   script.select_process();
}

public void primeiro_form() throws Exception {
   script.first_form();
}	
	
	public void segundo_form() throws Exception {
   script.second_form();
}	

public void tidyUp() {

script.tearDown();

}

}