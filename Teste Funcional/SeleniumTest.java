package BDDTest;

import cucumber.api.java.en.*;

import com.thoughtworks.selenium.*;

import java.util.concurrent.TimeUnit;

import static org.junit.Assert.*;

import org.openqa.selenium.*;

import org.openqa.selenium.firefox.FirefoxDriver;

import org.openqa.selenium.remote.RemoteWebDriver;

import java.io.FileInputStream;

import org.apache.poi.hssf.usermodel.HSSFCell;

import org.apache.poi.hssf.usermodel.HSSFRow;

import org.apache.poi.hssf.usermodel.HSSFSheet;

import org.apache.poi.hssf.usermodel.HSSFWorkbook;

 
public class SeleniumTest {

private WebDriver driver;

private String baseUrl;

private String browserName;

private String browserVersion;

@Before	
public void setUp() throws Exception {
driver = new FirefoxDriver();
baseUrl = "localhost:8080/bonita";//baseUrl = "http://bpm-si.inf.ufsm.br";
driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
}

@Test
	public void	efetua_login() throws Exception {
		driver.get(baseUrl+"/si/console/login.jsp?redirectUrl=%2Fsi%2Fconsole%2Fhomepage%3Fui%3Duser");
		
		driver.findElement(By.id("username")).clear();
		driver.findElement(By.id("username")).sendkeys("xxxxxx");
		
		driver.findElement(By.id("password")).clear();
		driver.findElement(By.id("password")).sendkeys("xxxxxx");
		//faz login
		driver.findElement(By.cssSelector("div.login_button_middle")).click();
		//aguarda a página carregar
		driver.manage().timeouts().implicityWait(60, TimeUnit.SECONDS);
		//verifica se o login estava correto
		driver.findElement(By.xpath("//div[@id='ProcessBrowserContainer']/table/tbody/tr[2]/td/div/tag/form/"));
		driver.switchTo().frame(driver.findElement(By.id("Solicitar_ACG--1.0")));
        Assert.assertTrue(selenium.isElementPresent("Solicitar_ACG--1.0"),"Usuário ou Senha Inválidos!");
	}
	
public void select_process(){
	driver.manage().timeouts().implicityWait(60, TimeUnit.SECONDS);
	driver.findElement(By.xpath("//div[@id='ProcessBrowserContainer']/table/tbody/tr[2]/td/div/tag/form/"));
	driver.switchTo().frame(driver.findElement(By.id("Solicitar_ACG--1.0")));
	driver.findElement(BY.cssSelector("button.bonita_form_button")).click();	
}
	
public void first_form(){	
	//busca o arquivo do excel com dados para teste
	FileInputStream fileInputStream = new FileInputStream("dadosteste.xls");

	workbook = new HSSFWorkbook(fileInputStream);
	HSSFSheet worksheet = workbook.getSheet(“Sheet1″);

//para cada linha no excel
	for (int i=0;i<=worksheet.getLastRowNum();i++){
		HSSFRow row1 = worksheet.getRow(i);
		HSSFCell cellA1 = row1.getCell((short) 0);
		String a1Val = cellA1.getStringCellValue();
		//aguarda a página carregar
		driver.manage().timeouts().implicityWait(60, TimeUnit.SECONDS);
		driver.switchTo().frame(driver.findElement(Bu.id("ProcessForm")));

		//nome
		driver.findElement(By.id("nome")).clear();
		driver.findElement(By.id("nome")).sendkeys(row1.getCell((short)0).getStringCellValue());

		//matricula
		driver.findElement(By.id("matricula")).clear();
		driver.findElement(By.id("matricula")).sendkeys(row1.getCell((short)1).getStringCellValue());
		//descricao
		driver.findElement(By.id("descricao")).clear();
		driver.findElement(By.id("descricao")).sendkeys(row1.getCell((short)2).getStringCellValue());
		//local
		driver.findElement(By.id("local")).clear();
		driver.findElement(By.id("local")).sendkeys(row1.getCell((short)3).getStringCellValue());
		//ch
		driver.findElement(By.id("ch")).clear();
		driver.findElement(By.id("ch")).sendkeys(row1.getCell((short)4).getStringCellValue());
		//data inicio
		driver.findElement(By.id("data_inicio")).clear();
		driver.findElement(By.id("data_inicio")).sendkeys(row1.getCell((short)5).getStringCellValue());
		//data fim
		driver.findElement(By.id("data_fim")).clear();
		driver.findElement(By.id("data_fim")).sendkeys(row1.getCell((short)6).getStringCellValue());
		// tipo da acg
		driver.findElement(By.id("tipo_acg")).clear();
		Selectselect= new Select (driver.findElement(By.id("tipo_acg")));
		select.selectByVisibleText(row1.getCell((short)7).getStringCellValue());
		//envia
		driver.findElement(BY.cssSelector("button.bonita_form_button")).click();
		driver.manage().timeouts().implicityWait(60, TimeUnit.SECONDS);
		driver.switchTo().frame(driver.findElement(By.id("ProcessForm")));
		//verifica se foi pro form 2
        Assert.assertTrue(selenium.isElementPresent("ProcessForm"),"Erro!");
		
		//aguarda a página carregar
		driver.manage().timeouts().implicityWait(60, TimeUnit.SECONDS);
		driver.switchTo().frame(driver.findElement(By.id("ProcessForm")));

		//segundo form
		//subtipo acg
		driver.findElement(By.id("subtipo_acg")).clear();
		Selectselect= new Select (driver.findElement(By.id("subtipo_acg")));
		select.selectByVisibleText(row1.getCell((short)8).getStringCellValue());
		//arquivo
		file_input = driver.find_element_by_id('arquivo');
		driver.execute_script('arguments[0].style = ""; arguments[0].style.display = "block"; arguments[0].style.visibility = "visible";', file_input);
		file_input.send_keys(row1.getCell((short)9).getStringCellValue());
		driver.findElement(BY.cssSelector("button.bonita_form_button")).click();
		
		river.manage().timeouts().implicityWait(60, TimeUnit.SECONDS);
        Assert.assertTrue(selenium.isElementPresent("Inbox"),"Erro!");
	}	
}
	
//public void second_form(){	
	//busca o arquivo do excel com dados para teste
//	FileInputStream fileInputStream = new FileInputStream("dadosteste.xls");

//	workbook = new HSSFWorkbook(fileInputStream);
//	HSSFSheet worksheet = workbook.getSheet(“Sheet1″);

//para cada linha no excel
//	for (int i=0;i<=worksheet.getLastRowNum();i++){		
//}
}
}
