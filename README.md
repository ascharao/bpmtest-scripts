# Testes automatizados de aplicações de BPMS #

## Teste de Carga com JMeter ##
A pasta *Teste de Carga* Contém um arquivo .jmx que apresenta o plano de teste de carga com as requisições capturadas.

Para executar o teste de carga deve-se:

1.  Importar esse arquivo para o JMeter;
2.  Realizar as alterações necessárias;
3.  Configurar as personalizações que desejar (número de usuários, tempo de execução de cada usuário, etc);
4.  Executar o teste.

## Teste funcional com Selenium + Cucumber ##

A pasta *Teste Funcional* contém os seguintes arquivos:

* Classe para executar o Cucumber (cukerunner.java);
* Arquivo com a definição do cenário de teste utilizando a linguagem Gherkin (test.feature), que nada mais é do que a definição, em ordem de execução, dos passos que são executados na aplicação bem como  os resultados esperados;
* Uma classe para definição dos passos executados no cenário (StepDefs.java);
* Uma classe com os métodos a serem executados em cada passo (SeleniumTest.java);

Para executar o teste funcional deve-se capturar a execução do processo através do Selenium e utilizar o código gerado (salvar como .java) para preencher e criar os métodos e passos dentro dos arquivos StepDefs.java e SeleniumTest.Java. O cenário deve ser criado dentro do arquivo test.feature.

Após a configuração dos arquivos, para executar o teste deve-se executar a classe cukerunner.java, esta classe abre uma nova janela no navegador que, se tudo foi configurado corretamente, segue reproduzindo os passos capturados com o Selenium aliados com os métodos modificados e, ao mesmo tempo, será exibido uma janela no WebDriver com as saídas do teste (erros, tempo de execução e etc).

## Processo testado ##

Nesta pasta encontram-se dois diagramas BPMN. Um deles não possui Lanes na definição do processo, pois estas não são interpretadas no BPMS Activiti.
Para executar o processo no BPMS Bonita Open Solution deve-se importar o arquivo DiagramaSolicitaACG.bpmn dentro do BPMS.
Para executar o processo no BPMS Activiti deve-se importar o arquivo DiagramaSolicitaACG (sem lanes).bpmn dentro do BPMS.